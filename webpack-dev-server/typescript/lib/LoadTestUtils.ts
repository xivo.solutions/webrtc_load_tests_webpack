import {
    ssl,
    port,
    audioTag,
    xucUrl,
    httpsAuthUrl,
    wssAuthUrl,
    proxyPort,
    loadtestUrl,
    xivoIp,
    mds1Ip,
    mds2Ip,
} from '../config/Config';
import {
    LineConfig,
    PhoneEvent,
    PhoneHintStatusEvent,
} from '../main';

declare var document;
declare var Cti;
declare var Callback;
declare var Membership;
declare var xc_webrtc;

const COOKIE_NAMES = {
    ITERATION: "iteration",
    SIPCALLID: "sipCallId",
}

let userToken: string;

export function attachReporters(username: string, userNumber: string, userType: string, partyNumber: string): void {
    attachPhoneEventReporter(username, userType, partyNumber, 'EventDialing');
    attachPhoneEventReporter(username, userType, partyNumber, 'EventRinging');
    attachPhoneEventReporter(username, userType, partyNumber, 'EventEstablished');
    attachPhoneEventReporter(username, userType, partyNumber, 'EventReleased');
    attachPhoneEventReporter(username, userType, partyNumber, 'EventOnHold');
    attachPhoneEventReporter(username, userType, partyNumber, 'EventFailure');
    attachPhoneHintStatusEventReporter(username, userNumber, userType, partyNumber, 4);
    attachPhoneHintStatusEventReporter(username, userNumber, userType, partyNumber, 0);
    attachPhoneHintStatusEventReporter(username, userNumber, userType, partyNumber, -1);
    attachPhoneHintStatusEventReporter(username, userNumber, userType, partyNumber, -2);
    attachPhoneHintStatusEventReporter(username, userNumber, userType, partyNumber, -99);
    attachSipCallIdReporter();
    return;
}

const attachSipCallIdReporter = (): void => {
    xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, function (e) {
        if (e.type === xc_webrtc.Incoming.CONNECTED) {
            storeInCookie(COOKIE_NAMES.SIPCALLID, e.sipCallId);
            return;
        }
        return;
    });
    return;
}

function attachPhoneEventReporter(username: string, userType: string, partyNumber: string, eventType: string): void {
    setPhoneHandler((e: PhoneEvent) => (e.eventType === eventType) &&
        reportToClientProxy(userType, username, partyNumber, eventType, `Reporting phone event: ${e.eventType}`))
    return;
}

function attachPhoneHintStatusEventReporter(username: string, userNumber: string, userType: string, partyNumber: string, eventType: number): void {
    setPhoneStatusHandler((e: PhoneHintStatusEvent) => (e.status === eventType && e.number === userNumber) &&
        reportToClientProxy(userType, username, partyNumber, translatePhoneStatusNumber(e), `Reporting phone status hint event: ${e.status}`));
    return;
}

// function attachWebrtcEventReporter(username: string, userType: string, partyNumber: string, eventType: string): void {
//     setPhoneHandler((e: PhoneEvent) => (e.eventType === eventType) &&
//         reportToClientProxy(userType, username, partyNumber, eventType, `Reporting phone event: ${e.eventType}`))
//     return;
// }

function setPhoneHandler(handler: (e: PhoneEvent) => void): void {
    Cti.setHandler(Cti.MessageType.PHONEEVENT, handler);
    return;
}

function setPhoneStatusHandler(handler: (e: PhoneHintStatusEvent) => void): void {
    Cti.setHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, handler);
    return;
}

// function setWebrtcEventHandler(handler: (e: WebrtcEvent) => void): void {
//     xc_webrtc.setHandler()
// }

function translatePhoneStatusNumber(e: PhoneHintStatusEvent): string {
    return (e.status > 0) ? 'INDISPONIBLE' : (e.status > -1) ? 'AVAILABLE' : (e.status > -2) ? 'DISCONNECTED' : (e.status < -2) ? 'ERROR' : 'UNEXISTING';
}

function reportToClientProxy(userType: string, username: string, partyNumber: string = 'None', eventType: string, logMessage?: string): void {
    (logMessage) && console.log(logMessage);
    let url = `http://${loadtestUrl}:${proxyPort}/report`
    url = url.concat(`?iteration=${getCurrentIteration()}`);
    url = url.concat(`&userType=${userType}`);
    url = url.concat(`&username=${username}`);
    url = url.concat(`&partyNumber=${partyNumber}`);
    url = url.concat(`&eventType=${eventType}`);
    url = (getFromCookies(COOKIE_NAMES.SIPCALLID).length > 1) ? url.concat(`&sipCallId=${getCurrentSipCallId()}`) : url;
    fetch(url, { mode: 'no-cors'});
    if (eventType === 'EventEstablished') optimiseAudioProcessing();
    return;
}

export function initOrIncrementIteration(): void {
    if (!document.cookie.includes(COOKIE_NAMES.ITERATION)) {
        storeInCookie(COOKIE_NAMES.ITERATION, '0');
        return;
    } else {
        let itNum = getCurrentIteration();
        itNum++;
        const iterationNumber = `${itNum}`;
        clearCookies();
        storeInCookie(COOKIE_NAMES.ITERATION, iterationNumber);
        return;
    }
}

function getCurrentIteration(): number {
    const iterationValue = getFromCookies(COOKIE_NAMES.ITERATION);
    const iteration = parseInt(iterationValue);
    return iteration;
}

function getCurrentSipCallId(): string {
    const sipCallId = getFromCookies(COOKIE_NAMES.SIPCALLID);
    return sipCallId;
}

function initCti(username: string, number: string): void {
    const wsUrl = `${wssAuthUrl}${userToken}`;
    Cti.WebSocket.init(wsUrl, username, number);
    Callback.init(Cti);
    Membership.init(Cti);
    return;
};

function processLineConfig(lineConfig: LineConfig, username: string): void {
    Cti.unsetHandler(Cti.MessageType.LINECONFIG, processLineConfig);
    // console.log(`Recieved line config: ${JSON.stringify(lineConfig)}`);
    xc_webrtc.initByLineConfig(lineConfig, username, ssl, port, userToken, audioTag, xucUrl);
    return;
};

export function getLineConfig(username: string): void {
    console.log('Requesting line config.')
    Cti.setHandler(Cti.MessageType.LINECONFIG, (lineConfig: LineConfig) => processLineConfig(lineConfig, username));
    Cti.getConfig('line');
    return;
}

function processLineConfigForCommonPeer(lineConfig: LineConfig, userId: string, peerName: string, peerNumber: string): void {
    Cti.unsetHandler(Cti.MessageType.LINECONFIG, processLineConfigForCommonPeer);
    // console.log(`Recieved line config: ${JSON.stringify(lineConfig)}; userId: ${userId}`);
    lineConfig["id"] = userId;
    lineConfig.hasDevice = false;
    lineConfig.isUa = false;
    lineConfig.mobileApp = false;
    lineConfig.name = peerName;
    lineConfig.password = peerName;
    lineConfig.number = peerNumber;
    // lineConfig.sipProxyName = (peerName.includes("caller")) ? "mds1" : "mds2";
    lineConfig.sipProxyName = "mds1"
    lineConfig.vendor = null;
    lineConfig.webRtc = true;
    // lineConfig.xivoIp = (peerName.includes("caller")) ? mds1Ip : mds2Ip;
    lineConfig.xivoIp = mds1Ip;
    // console.log(`Line config modification for common peer registration: ${JSON.stringify(lineConfig)}`);
    xc_webrtc.initByLineConfig(lineConfig, peerName, ssl, port, userToken, audioTag, xucUrl);
    return;
};

export function getLineConfigForCommonPeer(userId: string, peerName: string, peerNumber: string): void {
    console.log(`Requesting line config.; userId: ${userId}`)
    Cti.setHandler(Cti.MessageType.LINECONFIG, (lineConfig: LineConfig) => processLineConfigForCommonPeer(lineConfig, userId, peerName, peerNumber));
    Cti.getConfig('line');
    return;
}

export function authorize(reqObj: RequestInit, username: string, number: string): void {
    fetch(httpsAuthUrl, reqObj)
        .then((res) => res.json())
        .then((json) => {
            userToken = json.token;
            initCti(username, number);
            return;
        })
        .catch((error) => console.error(error));
};

function optimiseAudioProcessing(): void {
    const streamConstraints: Object = {
        audio: {
            mandatory: {
                googAutoGainControl: false,
                googAutoGainControl2: false,
                googEchoCancellation: false,
                googEchoCancellation2: false,
                googNoiseSuppression: false,
                googNoiseSuppression2: false,
                googHighpassFilter: false,
                googAudioMirroring: false,
                googTypingNoiseDetection: false,
            },
        }
    }
    window.navigator.mediaDevices.getUserMedia(streamConstraints);
    return;
}

const storeInCookie = (key: string, value: string): void => {
    document.cookie = `${key}=${value}`;
    return;
}

const getFromCookies = (key: string): string => {
    const cookies = document.cookie.split(";");
    const targetCookie = cookies.filter((cookie: string) => cookie.includes(key));
    if (targetCookie.length > 0) {
        const value = targetCookie[0].split("=")[1];
        return value;
    } else {
        return "";
    }
}

const clearCookies = (): void => {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i];
        const eqPos = cookie.indexOf("=");
        const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

export function initScenarioUserDataFromURL(urlQuery: string[]): any {
    const url: URL = getCurrentUrl();
    let userData: Object = {};
    urlQuery.forEach(query => {
        if (query == "delayBetweenCalls" || query == "callLength") {
            userData[query] = parseInt(getUrlParameter(url, query));
        } else {
            userData[query] = getUrlParameter(url, query);
        }
    });
    return userData;
}

function getCurrentUrl(): URL {
    const urlString: string = window.location.toString();
    const url = new URL(urlString);
    return url;
}

function getUrlParameter(url: URL, parameter: string): string {
    const param = url.searchParams.get(parameter);
    if (param == null) {
        (parameter != 'password') && console.log(`Parameter ${parameter} is not in URL ${url}`);
    }
    return param;
}