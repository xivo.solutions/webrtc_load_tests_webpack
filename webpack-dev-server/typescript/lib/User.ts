import {
    authorize,
} from "../lib/LoadTestUtils";

export class User {
    username: string;
    number: string;
    password: string;
    authHeaders: RequestInit;

    constructor(username: string, number: string, password?: string) {
        this.username = username;
        this.number = number;
        this.password = password || process.env.DEFAULT_USER_PWD;
        this.authHeaders = {
            "headers": {
                "accept": "application/json",
                "content-type": "application/json;charset=UTF-8",
            },
            "body": `{"login":"${this.username}","password":"${this.password}"}`,
            "method": "POST",
        };
    }

    login(): void {
        authorize(this.authHeaders, this.username, this.number);
    }
}

