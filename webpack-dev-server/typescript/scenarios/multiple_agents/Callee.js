"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User_1 = require("../../lib/User");
var LoadTestUtils_1 = require("../../lib/LoadTestUtils");
var urlQuery = [
    "username",
    "userType",
    "userNumber",
    "partyNumber",
    "callLength",
];
var _a = (0, LoadTestUtils_1.initScenarioUserDataFromURL)(urlQuery), username = _a.username, userType = _a.userType, userNumber = _a.userNumber, partyNumber = _a.partyNumber, callLength = _a.callLength;
(0, LoadTestUtils_1.initOrIncrementIteration)();
window['appVersion'] = "".concat(userType, ".").concat(username);
window.onbeforeunload = function () {
    xc_webrtc.stop();
    localStorage.clear();
};
function loadCallee() {
    Cti.setHandler(Cti.MessageType.LOGGEDON, function () { return startRegistration(); });
    (new User_1.User(username, userNumber)).login();
    return;
}
function startRegistration() {
    Cti.unsetHandler(Cti.MessageType.LOGGEDON, startRegistration);
    console.log("PhoneStatusUpdate: Setting handler for own phone ".concat(userNumber));
    Cti.setHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, function (e) { return finishRegistration(e); });
    (0, LoadTestUtils_1.attachReporters)(username, userNumber, userType, partyNumber);
    Cti.subscribeToPhoneHints([userNumber]);
    (0, LoadTestUtils_1.getLineConfig)(username);
    return;
}
;
function finishRegistration(e) {
    if (e.status === 0 && e.number === userNumber) {
        Cti.unsetHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, finishRegistration);
        Cti.unsubscribeFromAllPhoneHints();
        console.log("PhoneStatusUpdate: Own phone ".concat(userNumber, " ready - event.number: ").concat(e.number, " has event.status: ").concat(e.status));
        Cti.setHandler(Cti.MessageType.PHONEEVENT, function (e) { return onRing(e); });
        return;
    }
    return;
}
function onRing(e) {
    if (e.eventType === 'EventRinging') {
        Cti.unsetHandler(Cti.MessageType.PHONEEVENT, onRing);
        console.log('PhoneEventHandler: Ringing, going to answer.');
        Cti.answer();
        return;
    }
    return;
}
loadCallee();
