import { User } from '../../lib/User';
import { PhoneHintStatusEvent } from '../../main';
import {
    initScenarioUserDataFromURL,
    initOrIncrementIteration,
    getLineConfig,
    attachReporters,
} from '../../lib/LoadTestUtils';

declare var Cti;

function loadCaller() {
    const {
        username, userType, userNumber, partyNumber
    } = initScenarioUserDataFromURL();
    initOrIncrementIteration();
    Cti.setHandler(Cti.MessageType.LOGGEDON, () => startRegistration(username, partyNumber, userNumber, userType));
    const Caller = new User(username, userNumber);
    Caller.login();
}

function startRegistration(username: string, partyNumber: string, userNumber: string, userType: string): void {
    Cti.unsetHandler(Cti.MessageType.LOGGEDON, startRegistration);
    console.log(`PhoneStatusUpdate: Setting handler for own phone ${userNumber}`);
    Cti.setHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, (ev: PhoneHintStatusEvent) => finishRegistration(ev, partyNumber, userNumber));
    attachReporters(username, userNumber, userType, partyNumber);
    Cti.subscribeToPhoneHints([userNumber, partyNumber]);
    getLineConfig(username);
};

function finishRegistration(e: PhoneHintStatusEvent, partyNumber: string, userNumber: string): void {
    if (e.status === 0 && e.number === userNumber) {
        Cti.unsetHandler(Cti.MessageType.PHONEHINTSTATUSEVENT, finishRegistration);
        console.log(`PhoneStatusUpdate: Own phone ${userNumber} ready - event.number: ${e.number} has event.status: ${e.status}`);
    }
}

loadCaller();