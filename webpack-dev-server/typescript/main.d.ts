export interface MultipleCallsCallerData {
    username: string,
    userType: string,
    userNumber: string,
    partyNumber: string,
    callerCommonPeerId: string,
    callerCommonPeerName: string,
    callerCommonPeerNumber: string,
    delayBetweenCalls: number,
}

export interface MultipleCallsCalleeData {
    username: string,
    userType: string,
    userNumber: string,
    partyNumber: string,
    calleeCommonPeerId: string,
    calleeCommonPeerName: string,
    calleeCommonPeerNumber: string,
}

export interface MultipleUsersCallerData {
    username: string,
    userType: string,
    userNumber: string,
    partyNumber: string,
    firstNumber: string,
    lastNumber: string,
    delayBetweenCalls: number,
}

export interface MultipleUsersCalleeData {
    username: string,
    userType: string,
    userNumber: string,
    partyNumber: string,
    callLength: number,
}

export interface PhoneHintStatusEvent {
    number: string,
    status: number,
}

export interface PhoneEvent {
    eventType: string,
    DN: string,
    otherDN: string,
    otherDName: string,
    linkedId: string,
    uniqueId: string,
    queueName: string,
    callDirection: string,
    userData: XivoUserData,
    username: string
}

export interface XivoUserData {
    XIVO_CONTEXT: string,
    XIVO_USERID: string,
    XIVO_SRCNUM: string,
    XIVO_DSTNUM: string
}

export interface LineConfig {
    id: string,
    name: string,
    xivoIp?: string,
    hasDevice?: boolean,
    isUa?: boolean,
    webRtc?: boolean,
    vendor?: string,
    number?: string,
    mobileApp?: boolean,
    password: string,
    sipProxyName?: string
}

export interface SipEvent {
    type: string,
    data?: CallData,
    sipCallId?: string,
}

export interface CallData {
    caller: string
}

export interface xcWebrtcEvent {
    type: string,
    data: CalleeData,
    sipCallId: string,
}

export interface CalleeData {
    callee: string,
}