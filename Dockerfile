FROM node:14-slim

WORKDIR /webpack-dev-server/

COPY webpack-dev-server .

ENV SUT_FQDN=${SUT_FQDN}
ENV XIVOCC_IP=${XIVOCC_IP}

EXPOSE ${WEBPACK_PORT}

RUN apt-get update && apt-get install wget -y

ENTRYPOINT ./launch_webpack.sh
